/*
   - Create a function which is able to gather user details using prompt(). 
   - Create a function which is able to display simple data in the console.
   - Apply best practices in creating and defining functions by debugging erroneous code.

   Activity Output:
    1. prompt() and alert() used to show more interactivity in page.
	2.Values logged in console after invoking the function for objective  no.1

		Note: Name your own functions and variables but follow the conventions and best practice in naming functions and variables.
	3. Values shown in the console after invoking function created for objective 2.
	4. Values shown in the console after invoking function created for objective 3.
	5. Values shown in the console after invoking function debugged for objective 4.

	Activity Instruction:
	1. In the S17 folder, create an activity folder, an index.html file inside of it and link the index.js file.
	2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
	3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file.
	4.  Create a function which is able to prompt the user to provide their full name, age, and location. 
		- use prompt() and store the returned value into function scoped variables within the function. 
		- show an alert to thank the user for their input.
		- display the user's inputs in messages in the console.
 		- invoke the function to display the user’s information in the console.
		- follow the naming conventions for functions.
	5. Create a function which is able to print/display your top 5 favorite bands/musical artists. 
		- invoke the function to display your information in the console.
		- follow the naming conventions for functions.
	6. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating. 
		- look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
        - invoke the function to display your information in the console.
		- follow the naming conventions for functions.
	7. Debugging Practice - Debug the given codes and functions to avoid errors.
		- check the variable names.
		- check the variable scope.
		- check function invocation/declaration.
		- comment out unusable codes.
	8. Create a git repository named S17.
 	9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	10. Add the link in Boodle.

	How you will be evaluated
	1. No errors should be logged in the console.
	2. prompt() is used to gather information.
	3. alert() is used to show information.
	4. All values must be properly logged in the console.
	5. All variables are named appropriately and defines the value it contains.
	6. All functions are named appropriately and follows naming conventions.

 */

console.log("Hello World");

function getUserProfile() {
  const fullName = prompt("Please enter full name");
  const age = prompt("Please enter your age");
  const location = prompt("Please enter your address or location");

  alert("Thank you for giving us your information");

  printUserProfile({ fullName: fullName, age: age, location: location });
}

function printUserProfile(user) {
  printLog("Hello " + user.fullName);
  printLog("You are " + user.age + " years old.");
  printLog("You live in " + user.location);
}

getUserProfile();

function printFavoriteBands() {
  const favoriteBands = getFavoriteBands();
  printBands(favoriteBands);
}

function getFavoriteBands() {
  const favoriteBands = [
    "River Maya",
    "Eraserheads",
    "South Border",
    "Side A",
    "True Faith",
  ];
  return favoriteBands;
}

function printBands(favoriteBands) {
  let x = 1;
  for (const favoriteBand of favoriteBands) {
    printLog(x + ". " + favoriteBand);
    x++;
  }
}

printFavoriteBands();

function printFavoriteMoviesAndRating() {
  const favoriteMoviesWithRating = getFavoriteMoviesAndRating();
  printMoviesAndRating(favoriteMoviesWithRating);
}

function getFavoriteMoviesAndRating() {
  const favoriteMoviesWithRating = [
    { movie: "A Walk to Remember", rating: "98%" },
    { movie: "Transformer", rating: "95%" },
    { movie: "AntMan", rating: "96%" },
    { movie: "Avengers End Game", rating: "99%" },
    { movie: "100 Tula para kay Stella", rating: "94%" },
  ];

  return favoriteMoviesWithRating;
}

function printMoviesAndRating(favoriteMoviesWithRating) {
  let x = 1;
  for (const favoriteMovieWithRating of favoriteMoviesWithRating) {
    printLog(x + ". " + favoriteMovieWithRating.movie);
    printLog("Rotten Tomatoes Rating: " + favoriteMovieWithRating.rating);
    x++;
  }
}

printFavoriteMoviesAndRating();

let printFriends = function printUsers() {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  printLog("You are friends with:");
  printLog(friend1);
  printLog(friend2);
  printLog(friend3);
};

printFriends();
// console.log(friend1);
// console.log(friend2);

function printLog(message){
    console.log(message);
}
